package com.naxtylab.fallback

class FallbackAttemptsLimitExcaption(cause: Throwable) : Exception("Fallback attempts limit", cause)