package com.naxtylab.fallback

import com.naxtylab.fallback.mappers.FallbackMapper
import com.naxtylab.fallback.mappers.IndexMapper
import io.reactivex.*
import java.util.concurrent.TimeUnit

fun <T> Single<T>.retryWithFallback(
        strategy: (Long) -> Long,
        attempts: Int = INFINITY,
        timeUnit: TimeUnit = TimeUnit.SECONDS
): Single<T> {
    return retryWhen { errorsSource -> errorsSource.applyFallback(strategy, attempts, timeUnit) }
}

fun <T> Maybe<T>.retryWithFallback(
        strategy: (Long) -> Long,
        attempts: Int = INFINITY,
        timeUnit: TimeUnit = TimeUnit.SECONDS
): Maybe<T> {
    return retryWhen { errorsSource -> errorsSource.applyFallback(strategy, attempts, timeUnit) }
}

fun Completable.retryWithFallback(
        strategy: (Long) -> Long,
        attempts: Int = INFINITY,
        timeUnit: TimeUnit = TimeUnit.SECONDS
): Completable {
    return retryWhen { errorsSource -> errorsSource.applyFallback(strategy, attempts, timeUnit) }
}

fun <T> Flowable<T>.retryWithFallback(
        strategy: (Long) -> Long,
        attempts: Int = INFINITY,
        timeUnit: TimeUnit = TimeUnit.SECONDS
): Flowable<T> {
    return retryWhen { errorsSource -> errorsSource.applyFallback(strategy, attempts, timeUnit) }
}

fun <T> Observable<T>.retryWithFallback(
        strategy: (Long) -> Long,
        attempts: Int = INFINITY,
        timeUnit: TimeUnit = TimeUnit.SECONDS
): Observable<T> {
    return retryWhen { errorsSource ->
        errorsSource.map(IndexMapper())
                .concatMap { (error, attemptIdx) ->
                    if (INFINITY == attempts || attemptIdx < attempts) {
                        Observable.timer(strategy(attemptIdx), timeUnit)
                    } else {
                        Observable.error<Long>(FallbackAttemptsLimitExcaption(error))
                    }
                }
    }
}

private fun Flowable<Throwable>.applyFallback(
        strategy: (Long) -> Long,
        attempts: Int = INFINITY,
        timeUnit: TimeUnit = TimeUnit.SECONDS
): Flowable<Long> {
    val fallbackMapper = FallbackMapper(strategy, attempts, timeUnit)
    return map(IndexMapper()).concatMap(fallbackMapper)
}