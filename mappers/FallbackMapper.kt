package com.naxtylab.fallback.mappers

import com.naxtylab.fallback.FallbackAttemptsLimitExcaption
import com.naxtylab.fallback.INFINITY
import io.reactivex.Flowable
import io.reactivex.functions.Function
import java.util.concurrent.TimeUnit

internal class FallbackMapper(
        val strategy: (Long) -> Long,
        val attempts: Int = INFINITY,
        val timeUnit: TimeUnit = TimeUnit.SECONDS
) : Function<Pair<Throwable, Long>, Flowable<Long>> {

    override fun apply(fallbackAttempt: Pair<Throwable, Long>): Flowable<Long> {
        val (error, attemptIdx) = fallbackAttempt
        return if (INFINITY == attempts || attemptIdx < attempts) {
            Flowable.timer(strategy(attemptIdx), timeUnit)
        } else {
            Flowable.error(FallbackAttemptsLimitExcaption(error))
        }
    }

}