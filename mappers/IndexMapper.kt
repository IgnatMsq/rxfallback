package com.naxtylab.fallback.mappers

import io.reactivex.functions.Function

internal class IndexMapper<T> : Function<T, Pair<T, Long>> {

    var idx: Long = 0L

    override fun apply(t: T): Pair<T, Long> {
        val currentIdx = idx
        idx++
        return Pair(t, currentIdx)
    }
}
