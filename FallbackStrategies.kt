package com.naxtylab.fallback

val INFINITY = -1

val LINEAR_FALLBACK = { attempt: Long -> attempt }

val POWER_OF_TWO_FALLBACK = { attempt: Long -> Math.pow(2.0, attempt.toDouble()).toLong() }

val FIBONACCI_FALLBACK = { attempt: Long -> fibonacci(attempt + 1) }

private fun fibonacci(n: Long) =
        (2 until n).fold(1 to 1) { (prev, curr), _ ->
            curr to (prev + curr)
        }.second